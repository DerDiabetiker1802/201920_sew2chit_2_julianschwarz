﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snake
{
    public enum Richtung { UP, DOWN, RIGHT, LEFT };

    class SnakeHead : Item
    {

        public SnakeHead(int x, int y, char symbol) : base(x, y, symbol)
        {
           
        }

        public override void DrawOnConsole()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            base.DrawOnConsole();
        }

        public void Move(Richtung direction)
        {
            DeleteFromConsole();

            {
                if (direction == Richtung.UP) Y--;
                if (direction == Richtung.DOWN) Y++;
                if (direction == Richtung.RIGHT) X++;
                if (direction == Richtung.LEFT) X--;

            }
            

        }
    }
}
