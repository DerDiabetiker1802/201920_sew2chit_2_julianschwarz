﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snake
{
    class Futter : Item
    {
        public Futter(int x, int y, char symbol) : base(x, y, symbol)
        {

        }

        public override void DrawOnConsole()
        {
            Console.BackgroundColor = ConsoleColor.Red;
            base.DrawOnConsole();
        }
    }
}