﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snake
{
    class Item
    {
        public int X { get; set; }

        public int Y { get; set; }

        private char Symbol;

        public ConsoleColor old = Console.BackgroundColor;
        public Item(int x, int y, char symbol)
        {
            X = x;
            Y = y;
            Symbol = symbol;
        }

        public virtual void DrawOnConsole()
        {
            Console.SetCursorPosition(X, Y);
            Console.WriteLine(Symbol);
            Console.BackgroundColor = old;
        }

        public void DeleteFromConsole()
        {
            Console.SetCursorPosition(X, Y);
            Console.WriteLine(" ");
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}