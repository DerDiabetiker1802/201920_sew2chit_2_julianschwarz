﻿using System;

namespace Snake
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Random rnd = new Random();
            ConsoleKeyInfo cki = new ConsoleKeyInfo();
            SnakeHead head = new SnakeHead(Console.WindowWidth / 2, Console.WindowHeight / 2, 'x');
            int pkt = 0;
            Futter f1 = new Futter(rnd.Next(1, Console.WindowWidth - 1), rnd.Next(1, Console.WindowHeight - 1), 'o');
            //Futter f2 = new Futter(25, 20, 'o', 5);
            //Futter f3 = new Futter(10, 22, 'o', 2);
            //Futter f4 = new Futter(20, 15, 'o', 3);

            head.DrawOnConsole();

            f1.DrawOnConsole();
            //f2.DrawOnConsole();
            //f3.DrawOnConsole();
            //f4.DrawOnConsole();

            Console.SetCursorPosition(1, 1);
            Console.WriteLine("Punkte: " + pkt);
            do
            {
                System.Threading.Thread.Sleep(100);
                if (Console.KeyAvailable)
                {
                    cki = Console.ReadKey();

                }
                ueberg(cki, head);
                if (head.X == f1.X && head.Y == f1.Y)
                {
                    f1 = new Futter(rnd.Next(1, Console.WindowWidth - 1), rnd.Next(1, Console.WindowHeight - 1), 'o');
                    f1.DrawOnConsole();
                }

            }
            while (head.X <= Console.WindowWidth  && head.X >= 1 && head.Y <= Console.WindowHeight  && head.Y >= 1);
            Console.Clear();
            Console.SetCursorPosition(Console.WindowWidth / 2 - 5, Console.WindowHeight / 2);
            Console.WriteLine("Meeep Verloren");
            System.Threading.Thread.Sleep(4000);
        }
        static void ueberg(ConsoleKeyInfo cki, SnakeHead head)
        {
            switch (cki.Key)
            {
                case ConsoleKey.UpArrow: head.Move(Richtung.UP); break;
                case ConsoleKey.DownArrow: head.Move(Richtung.DOWN); break;
                case ConsoleKey.LeftArrow: head.Move(Richtung.LEFT); break;
                case ConsoleKey.RightArrow: head.Move(Richtung.RIGHT); break;
            }
            head.DrawOnConsole();
        }
    }
}