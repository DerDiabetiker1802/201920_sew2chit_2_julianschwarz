﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsTicTacToe
{
    public partial class TicTacToe : Form
    {
        string spieler = "x";
        string[,] spielstand = new string[3, 3];
        TTT ttt = new TTT();

        public TicTacToe()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Button clicked = sender as Button;
            clicked.Text = spieler;
            //spieler = spieler == "x" ? "o": "x";
            clicked.Enabled = false;

            MessageBox.Show(clicked.Name);

            //zerlege de clickedName - dann kommt eine Zahl heraus 
            //diese Zahl entspricht jenem Feld das angeklickt worden ist
            // 1 2 3
            // 4 5 6
            // 7 8 9

            //z.b. bei 6 setze ich spielstand[2,1]   [x,y]
            //Gewinnermittlung wie im Vorjahr

            string nr = clicked.Name.Substring(clicked.Name.Length - 1);
            ttt.Setze(Convert.ToInt32(nr), spieler);

            string sieger = ttt.Gewinnermittlung();
            if(sieger != null)
            {
                MessageBox.Show(sieger + " hat das Spiel gewonnen");
            }

            //Spielerwechsel
            spieler = spieler == "x" ? "o" : "x";

        }
    }
}
