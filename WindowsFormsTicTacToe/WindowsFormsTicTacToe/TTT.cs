﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsTicTacToe
{
    class TTT
    {
        string[,] feld = new string[3, 3];

        public void Setze(int nr, string wert)
        {
            int x = (nr - 1) % 3;
            int y = (nr - 1) / 3;

            feld[x, y] = wert;
        }
    
        public string Gewinnermittlung()
        {
            string wag;
            string senk;
            string dia;

            for(int i = 0; i< 2; i++)
            {
                wag = GewinnWaagrecht(i);
                if (wag != null)
                return wag;
            }
        
            for(int i= 0; i < 2; i++)
            {
                senk = GewinnSenkrecht(i);
                if (senk != null)
                return senk;
            }


            for (int i = 0; i < 2; i++)
            {
                dia = GewinnDiagonal(i);
                if (dia != null)
                return dia;
            }
            return null;
        }
    
        public string GewinnWaagrecht(int zeile)
        {
            if (feld[0, zeile] != "")
                if (feld[0, zeile] == feld[1, zeile] && feld[1, zeile] == feld[2, zeile])
                    return feld[0, zeile];

            return null;
        }
    
        public string GewinnSenkrecht(int spalte)
        {
            if (feld[spalte, 0] != "")
                if (feld[spalte, 0] == feld[spalte, 1] && feld[spalte, 1] == feld[spalte, 2])
                    return feld[spalte, 0];

            return null;
        }

        public string GewinnDiagonal(int diagonal)
        {
            if (feld[1, 1] != "")
                if (feld[0, 0] == feld[1, 1] && feld[1, 1] == feld[2, 2])
                    return feld[0, 0];


            if (feld[2, 0] == feld[1, 1] && feld[1, 1] == feld[0, 2])
                return feld[1, 1];
                return null;
        }
    }
}
