﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Schiffe_versenken_Forms
{
    public partial class Form1 : Form
    {
        const int XSIZE = 15;
        const int YSIZE = 10;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int y = 0; y < YSIZE; y++)
            {
                for (int x = 0; x < XSIZE; x++)
                {
                    Button b = new Button();

                    b.Left = 20 + x * 50;

                    b.Top = 20 + x * 50;

                    b.Tag = y * XSIZE + x + 1;

                    b.Width = 40;

                    this.Controls.Add(b);

                    b.Click += B_Click;
                }
            }
        }
        private void B_Click(object sender, EventArgs e)
        {
            Button clicked = sender as Button;
            int nr = Convert.ToInt32(clicked.Tag.ToString());
            Tuple<int, int> erg = vs.Setze(nr, "");
            clicked.Text = erg.Item1.ToString();
            clicked.Enabled = false;
        }
    }
}
