﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schiffe_versenken_Forms
{
    class vs
    {
        interface ISpielbar
        {
            Tuple<int, int> Setze(int nr, string wert);
          
        }
        public class Versenken : ISpielbar
        {
            const int XSIZE = 15;
            const int YSIZE = 10;
            private string[,] feld = new string[XSIZE, YSIZE];

         
            public Tuple<int,int> Setze(int nr,string wert)
            {
                int x = (nr - 1) % XSIZE;
                for (int y = 0; y < YSIZE -1; y--)
                {
                    if (feld[x,y] == null)
                    {
                        feld[x, y] = wert;
                        return new Tuple<int, int>(x, y);
                    }
                }
                return null;
            }
            


        }

        internal static Tuple<int, int> Setze(int nr, string v)
        {
            throw new NotImplementedException();
        }
    }
}
