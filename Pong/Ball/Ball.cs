﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pong
{
    public class Ball
    {
        public double X { get; set; }
        public double Y { get; set; }

        double w, h;
        Random r = new Random();

        public Ball(int x, int y)
        {
            this.X = x;
            this.Y = y;
            w = r.NextDouble(); h = r.NextDouble() / 2;

        }
        public void Move()
        {
            X += w;
            Y += h;
            if (X <= 1) w = -w;
            if (Y <= 1) h = -h;
            if (X >= Console.WindowWidth - 2) w = -w;
            if (Y >= Console.WindowHeight - 2) h = -h;

        }
    }
}
