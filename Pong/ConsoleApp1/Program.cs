﻿using System;

namespace Pong
{
    class Program
    {
        static void Main(string[] args)
        {
            Ball b = new Ball(1 , 1);
            int lastx = 0;
            int lasty = 0;
            Console.CursorVisible = false;
            while (true)
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo c = Console.ReadKey();
                    Console.WriteLine((char)8);
                    Console.WriteLine(" ");
                    Console.SetCursorPosition(0, 0);
                    Console.Write("                            ");
                    Console.SetCursorPosition(0, 0);
                    Console.Write("gedrückt" + c.Key.ToString());
                }
                System.Threading.Thread.Sleep(50);

                b.Move();
                if (lastx != (int)b.X || lasty != (int)b.Y)
                {
                    Console.SetCursorPosition(lastx, lasty);
                    Console.Write(" ");
                    Console.SetCursorPosition((int)b.X, (int)b.Y);
                    Console.Write("o");
                }
                lasty = (int)b.Y;
                lastx = (int)b.X;
            }
        }
        
    }
}
