﻿using System;
using System.Drawing;
using System.Windows.Forms;
using VG;


namespace WindowsForms4Gewinnt
{
    public partial class Form1 : Form
    {

        int zuege = 0;

        string player = "Gelb";
        Tuple_.Ermittlung vg = new Tuple_.Ermittlung();
        public Form1()
        {
            InitializeComponent();
        }
        PictureBox[,] pcbs;
        private void pictureBox36_Click(object sender, EventArgs e)
        {
           
            zuege++;

            PictureBox clicked = sender as PictureBox; 

            string nr = clicked.Name.Substring(10);
            int n = Convert.ToInt32(nr);        //pictureBoxnummer erstellen
            Tuple<int, int> werte = vg.Setze(n, player);               //und Spielstein in der Logik setzen

           

            //pcbs[werte.Item1, werte.Item2].Enabled = false;
            if (werte != null) 
            {
                if (player == "Gelb") pcbs[werte.Item1, werte.Item2].Image = Image.FromFile("Feld_Gelb.png"); // Wer hat gewonnen?
                else pcbs[werte.Item1, werte.Item2].Image = Image.FromFile("Feld_Rot.png");
            }

            //pcbs[werte.Item1, werte.Item2].Text = player;


            string winner = vg.Gewinnermittlung(); //hat jemand gewonnen??
            if (zuege == 42) // Alle Zuege abgeschlossen
            {
                label1.Text = "Unentschieden!";
                MessageBox.Show("Unentschieden");
                System.Threading.Thread.Sleep(2000);
                MessageBox.Show("Automatisches Schliesen in 1ner Sekunde");
                System.Threading.Thread.Sleep(1000);
                Close();
            }
            if (winner != null)
            {
                label1.Text = winner + " gewinnt!";
                MessageBox.Show(winner + " hat gewonnen!"); //Gewinner ausgeben und restlichen Pictureboxen deaktivieren
                pictureBox1.Enabled = false;
                pictureBox2.Enabled = false;
                pictureBox3.Enabled = false;
                pictureBox4.Enabled = false;    
                pictureBox5.Enabled = false;
                pictureBox6.Enabled = false;
                pictureBox7.Enabled = false;
                System.Threading.Thread.Sleep(2000);
                MessageBox.Show("Automatisches Schliesen in 1ner Sekunde");
                System.Threading.Thread.Sleep(1000);
                Close();

            }
           
            //Spielerwechsel
            player = player == "Gelb" ? "Rot" : "Gelb";
            label2.Text = "Spielzug " + zuege + "/42";


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PictureBox[,] pcb = {
                {pictureBox1,pictureBox8,pictureBox15,pictureBox22,pictureBox29,pictureBox36},
                {pictureBox2,pictureBox9,pictureBox16,pictureBox23,pictureBox30,pictureBox37},
                {pictureBox3,pictureBox10,pictureBox17,pictureBox24,pictureBox31,pictureBox38},
                {pictureBox4,pictureBox11,pictureBox18,pictureBox25,pictureBox32,pictureBox39},
                {pictureBox5,pictureBox12,pictureBox19,pictureBox26,pictureBox33,pictureBox40},
                {pictureBox6,pictureBox13,pictureBox20,pictureBox27,pictureBox34,pictureBox41},
                {pictureBox7,pictureBox14,pictureBox21,pictureBox28,pictureBox35,pictureBox42} };
            pcbs = pcb;
        }

    
        private void button3_Click_1(object sender, EventArgs e)
        {
           
            MessageBox.Show("Zum Verlassen 'ok' drücken oder das Fenster schliesen");
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
