﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace VG
{
    class Tuple_
    {
        interface ISpielbar
        {
            Tuple<int, int> Setze(int nr, string wert);
            string Gewinnermittlung();
        }

        public class Ermittlung : ISpielbar
        {
            const int XSIZE = 7, YSIZE = 6;
            private string[,] feld = new string[XSIZE, YSIZE];
            public Tuple<int, int> Setze(int nr, string wert)
            {
                // 1  2  3  4  5  6  7          0  1  2  3   4   5   6
                // 8  9 10 11 12 13 14          7  8  9 10  11  12  13
                //15 16 17 18 19 20 21         14 15 16 17  18  19  20
                //22 23 24 25 26 27 28         21 22 23 24  25  26  27
                //29 30 31 32 33 34 35         28 29 30 31  32  33  34
                //36 37 38 39 40 41 42         35 36 37 38  39  40  41



                int x = (nr - 1) % XSIZE;

                for (int y = YSIZE -1; y >= 0; y--)
                {
                    if (feld[x, y] == null)
                    {
                        feld[x, y] = wert;
                        return new Tuple<int, int>(x, y);
                    }
                }

                return null;
            }

            public string Gewinnermittlung()
            {
                string g;
                for (int i = 0; i < YSIZE; i++)
                {
                    g = GewinnWaagrecht(i);
                    if (g != null)
                        return g;
                }

                for (int i = 0; i < XSIZE; i++)
                {
                    g = GewinnSenkrecht(i);
                    if (g != null)
                        return g;
                }

                g = HauptDiagonale();
                if (g != null)
                    return g;

                g = NebenDiagonale();
                if (g != null)
                    return g;

                return null;
            }

            private string GewinnWaagrecht(int zeile)
            {
                for (int x = 0; x < 4; x++) // 4 Möglichkeiten je Zeile
                    if (feld[x, zeile] != null)                         //nicht leer
                        if (feld[x, zeile] == feld[x + 1, zeile]        //2
                            && feld[x, zeile] == feld[x + 2, zeile]     //3
                            && feld[x, zeile] == feld[x + 3, zeile])    //4
                            return feld[x, zeile]; //der Gewinner
                return null; //noch kein Gewinner
            }

            private string GewinnSenkrecht(int spalte)
            {
                for (int y = 0; y < 3; y++) //3 Möglichkeiten je Spalte
                    if (feld[spalte, y] != null)                            //nicht leer
                        if (feld[spalte, y] == feld[spalte, y + 1]          //2
                            && feld[spalte, y] == feld[spalte, y + 2]       //3
                            && feld[spalte, y] == feld[spalte, y + 3])      //4
                            return feld[spalte, y]; // der Gewinner
                return null; //noch kein Gewinner
            }

            private string HauptDiagonale()
            {
                for (int x = 0; x < 4; x++) // 4 Möglichkeiten je Zeile
                {
                    for (int y = 0; y < 3; y++) // 3 Möglichkeiten je Spalte
                    {
                        if (feld[x, y] != null)                         // nicht leer
                            if (feld[x, y] == feld[x + 1, y + 1]        //2
                                && feld[x, y] == feld[x + 2, y + 2]     //3
                                && feld[x, y] == feld[x + 3, y + 3])    //4
                                return feld[x, y]; // der Gewinner
                    }
                }
                return null; //noch kein Gewinner
            }

            private string NebenDiagonale()
            {

                for (int x = 6; x > 2; x--) //4 Möglichkeiten je Zeile von ganz rechts
                {
                    for (int y = 0; y < 3; y++) //3 Möglichkeiten je Spalte
                    {
                        if (feld[x, y] != null)                         // nicht leer
                            if (feld[x, y] == feld[x - 1, y + 1]        //2 
                                && feld[x, y] == feld[x - 2, y + 2]     //3
                                && feld[x, y] == feld[x - 3, y + 3])    //4
                                return feld[x, y]; //der Gewinner

                    }
                }
                return null; //noch kein Gewinner
            }
        }
    }
}
